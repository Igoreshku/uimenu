using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioClip volume;
    public AudioSource volum;
    public AudioSource music;
    private bool musicenable = true;
    private bool volumenable = true;

    private void Start()
    {


        
    }

    public void OnOffVolume()
    {
        if (volumenable)
        {
            volum.enabled = true;
            volumenable = false;
        }

        else
        {
            volum.enabled = false;
            volumenable = true;
        }

    }

    public void OnOffMusic()
    {
        if (musicenable)
        {
            music.enabled = true;
            musicenable = false;
        }

        else
        {
            music.enabled = false;
            musicenable = true;
        }
            
    }

    

    public void PressButton()
    {
            volum.PlayOneShot(volume);

    }

    
}
