using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayMap : MonoBehaviour
{
    public Button[] lvl;
    public GameObject[] number;
    public GameObject[] corrent;
    public GameObject[] locking;
    public GameObject panel;
    
    
    

    void Start()
    {
        
        
        for (int i = 0; i < lvl.Length; i++)
            if(i > 5)
                lvl[i].interactable = false;
    }

    private void Update()
    {


    }

    public void Panel()
    {
        panel.SetActive(false);
    }

    public void OnDestroy()
    {
        
    }

    public void StartLvl(int LvlIndex)
    {
        if(lvl[LvlIndex].interactable == true)
        {
            SceneManager.LoadScene(1);
            lvl[LvlIndex].interactable = false;
            number[LvlIndex].SetActive(false);
            corrent[LvlIndex].SetActive(true);
            ButtonsMenu.ticket += 100;
            ButtonsMenu.count++;
            
        }
        
        if (LvlIndex >= 5 && LvlIndex < lvl.Length - 2)
        {
            lvl[LvlIndex + 1].interactable = true;
            locking[LvlIndex - 5].SetActive(false);
            number[LvlIndex + 1].SetActive(true);
            
        }
        if (LvlIndex == lvl.Length - 2)
        {
            lvl[LvlIndex + 1].interactable = true;
            locking[LvlIndex - 5].SetActive(false);
            number[LvlIndex+1].SetActive(true);
        }
    }
}
