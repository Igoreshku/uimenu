using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gift : MonoBehaviour
{
    [SerializeField]public Button[] gift;
    public GameObject panel;
    public float hour;
    public static int date = 0;
    private int prizex5 = 5;
    private int prizex10 = 10;
    private int prizex15 = 15;


    void Start()
    {
        
        hour = PlayerPrefs.GetInt("days");
        hour = System.DateTime.Now.Hour;
        for (int i = 0; i < gift.Length; i++)
            if (i >= 1)
                gift[i].interactable = false;
    }

    
    public void Update()
    {
       
    }

    public void Day()
    {
        if (hour == 0)
        {
            date++;
            gift[date-1].interactable = true;
            if (date == 7)
                date = 1;
        }
    }

    public void Prize(int number)
    {
        if(date <= 2 || date == 7)
        {

            ButtonsMenu.ticket *= prizex5;
            gift[number].interactable = false;
        }
        if(date ==3 || date == 4)
        {
            ButtonsMenu.ticket *= prizex10;
            gift[number].interactable = false;
        }
        if (date == 5 || date == 6)
        {
            ButtonsMenu.ticket *= prizex15;
            gift[number].interactable = false;
        }
    }

    public void Panel()
    {
        panel.SetActive(false);
    }
}
