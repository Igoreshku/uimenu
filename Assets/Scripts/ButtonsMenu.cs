using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class ButtonsMenu : MonoBehaviour
{
    public TextMeshProUGUI tickettext;
    public TextMeshProUGUI gifttext;
    public GameObject settings;
    public GameObject giftback;
    public GameObject gift7;
    public GameObject market;
    public GameObject MapPlay;
    public GameObject gift;
    public Image greenline;
    public static int ticket = 0;
    public static int count = 0;
    public float green = 0f;

    public void Start()
    {
        

    }

    void Update()
    {
        tickettext.text = ticket.ToString();
        gifttext.text = (Gift.date) + "/7";
        green = (float)Gift.date / 7;
        greenline.fillAmount = green;
    }

    public void Market()
    {
        market.SetActive(true);
    }

    public void Settings()
    {
        settings.SetActive(true);
    }

    public void GiftButton()
    {
        if(Gift.date < 6)
        {
            giftback.SetActive(true);
            gift.SetActive(true);
            
        } 
        if (Gift.date == 6)
        {
            gift.SetActive(false);
            giftback.SetActive(true);
            gift7.SetActive(true);
        }
    }

    public void Play()
    {
        MapPlay.SetActive(true);
    }
}
