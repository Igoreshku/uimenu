using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
public class Chests : MonoBehaviour
{
    public void Complete(Product product)
    {
        switch (product.definition.id)
        {
            case "com.menu.chest.give500":
                AddCoins500();
                break;
            case "com.menu.chest.give1000":
                AddCoins1000();
                break;
        }
    }

    private void AddCoins500()
    {
        ButtonsMenu.ticket += 500;
    }

    private void AddCoins1000()
    {
        ButtonsMenu.ticket += 1000;
    }

}


