using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static PlayMap;

public class Shop : MonoBehaviour
{
    public GameObject[] corrent;
    public GameObject[] sum;
    public GameObject[] ticketimage;
    public GameObject[] Icon1;
    public GameObject[] Icon2;
    public GameObject corrent1;
    public GameObject sum1;
    public GameObject ticketimage1;
    public GameObject panel;
    private int price = 500;

    

    private void Awake()
    {
        
    }

    void Update()
    {
        if (ButtonsMenu.count >= 10)
        {
            for (int i = 0; i < 2; i++)
            {
                Icon1[i].SetActive(false);
                Icon2[i].SetActive(true);
            }
        }
    }

    public void Panel()
    {
        panel.SetActive(false);
    }
    
    public void Buy()
    {
        
        if(ButtonsMenu.ticket >= price)
        {
            corrent1.SetActive(true);
            sum1.SetActive(false);
            ticketimage1.SetActive(false);
            ButtonsMenu.ticket -= price;
        }
    }
    public void Buy10lvl(int number)
    {
        if (ButtonsMenu.count >= 10 && ButtonsMenu.ticket >= price)
        {
            corrent[number-1].SetActive(true);
            sum[number-1].SetActive(false);
            ticketimage[number-1].SetActive(false);
            ButtonsMenu.ticket -= price;
        }
    }
    
    public void Chest(int prize)
    {
        ButtonsMenu.ticket += prize;
    }
}
